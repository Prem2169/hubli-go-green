/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */
$(function () {
   $("#owl-carousel-statement").owlCarousel({
      items: 1,
       autoplay: true, //ghumta rahega iske wajah se
       smartSpeed: 700, //How many Millisecond
       loop: true, //peche se agge loop ke wajah se
       autoplayHoverPause: true, //jitne der hover hai utni der pause rahega
       dots: false, //left right navigation button true,
       responsive: {
           //0 or up wala width
           0: {
               items: 1 //no. of items to be shown at the width it will show 1 item at width 0 to 480
           }, 
           480: {
               items: 1 //it will show 2 items at width 480 above
           }
       }
   }); 
});

$(function () {
   $("#card-company").owlCarousel({
      items: 4,
       autoplay: false, //ghumta rahega iske wajah se
       smartSpeed: 700, //How many Millisecond
       loop: true, //peche se agge loop ke wajah se
       dots: false, //by default true hota hai dots wo slideshow ke neeceh ata hai
       responsive: {
           //0 or up wala width
           0: {
               items: 2 
           }, 
           480: {
               items: 4
           }
       }
   }); 
    $('#progress-elements').waypoint(function () {
       $('.progress-bar').each(function () {
          $(this).animate({
              width: $(this).attr('aria-valuenow') + "%"
          }, 800); 
       });
        this.destroy();
    },{
        offset: 'bottom-in-view'
//        jidr div khatam ho raha hai waha pe ye animation start hoga
    }
);
});


document.getElementById('stop').addEventListener('hover', changeStop());
function changeStop(){
//    document.getElementById('stop').innerHTML = "<img class='item-img' src='img/icon-1.png' alt=''><h4 class='item-head'>Stop Monster Boats</h4><p class='item-content'>Capitalize on low hanging fruit ot the identify a ballpark value added Most activity to beta test.</p>";
//   
    
    document.getElementById('stop').attr('src').innerHTML="img/icon-1.png";
}